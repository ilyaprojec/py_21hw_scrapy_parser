from pymongo import MongoClient
import scrapy
import re

client = MongoClient('mongodb://root:example123@localhost:27017/')
db = client['220volt']

collection = db['categories']


class ExampleSpider(scrapy.Spider):
    name = 'spider'

    start_urls = ['https://220volt.com.ua/', ]

    def parse_product(self, response, **kwargs):
        price_block = response.css('.product_page > .price_block > div')[0]
        specifications_grids = response.css('.grid-372 > .featers_list')

        category = response.css('.breadcrumbs > div > span')[1].css('a::text').get()
        prod_name = response.css('#prod_name_kred::text').get()
        specifications = []

        for grid in specifications_grids:
            specifications.extend(grid.css('li'))

        fields = {
            'category': category,
            'product': re.sub(r"^\s+|\s+$", "", prod_name),
        }
        specs_fields = []

        def add_spec(spec_val):
            specs_fields.append({re.sub(r"^\s+|\s+$|[\.\:]", "", spec_name): re.sub(r"^\s+|\s+$", "", spec_val)})

        def add_price(price_val):
            fields.update({'price': price_val})

        if price_block.css('.prices'):
            add_price(re.sub(r"^\s+|\s+$", "", price_block.css('.prices > #price_kred::text').get()) +
                      " " + re.sub(r"^\s+|\s+$", "", price_block.css('.prices > .currency::text').get()))
        else:
            add_price(re.sub(r"^\s+|\s+$", "", price_block.css('.ask_price::text').get()))

        for specification in specifications:
            spec_name = specification.css('::text').get()

            if specification.css('a'):
                add_spec(specification.css('a::text').get())
            else:
                add_spec(specification.css('span::text').get())

        fields.update({'specifications': specs_fields})
        collection.insert_one(fields)

    def parse_category(self, response, **kwargs):
        products = response.css('.justify > .item-product')

        for product in products:
            yield response.follow(product.css('.relative > a')[0], self.parse_product)

        if response.css('a.next_page'):
            yield response.follow(response.css('a.next_page')[0], self.parse_category)

    def parse(self, response, **kwargs):
        categories = response.css('.catalogmenu > ul > li')

        for category in categories:
            if categories.index(category) > 1:
                break
            else:
                yield response.follow(category.css('a')[0], self.parse_category)
